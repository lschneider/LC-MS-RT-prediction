import pyopenms as oms
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import scipy.signal
from PIL.ImageOps import cover


def compute_chromatograms(rt, mz, intensity, start_c, end_c):
    value=[]

    for k in range(len(rt)):
        c = np.array(mz[k])
        i = np.array(intensity[k])
        value.append(np.sum(np.where((end_c > c) & (c > start_c), i, 0)))

    return value

def get_df(expe, long: bool = False):
    """Generates a pandas DataFrame with all peaks in the MSExperiment

    Parameters:
    long: set to True if you want to have a long/expanded/melted dataframe with one row per peak. Faster but
        replicated RT information. If False, returns rows in the style: rt, _np.array(mz), _np.array(int)

    Returns:
    pandas.DataFrame: feature information stored in a DataFrame
    """


    cols = ["RT", "mzarray", "intarray", 'MSlevel','MS1_mz_min','MS1_mz_max']

    df = pd.DataFrame(data=((spec.getRT(), *spec.get_peaks(), spec.getMSLevel(),
                             spec.getPrecursors()[0].getMZ() - spec.getPrecursors()[0].getIsolationWindowLowerOffset() if  spec.getMSLevel()==2 else None,
                             spec.getPrecursors()[0].getMZ() + spec.getPrecursors()[0].getIsolationWindowUpperOffset() if  spec.getMSLevel()==2 else None
                             ) for spec in expe), columns=cols)

    if long:
        RT = []
        mz = []
        inty = []
        ms_lv = []
        ms1_mz_min = []
        ms1_mz_max = []
        for index, row in df.iterrows():
            mz.extend(row['mzarray'])
            inty.extend(row['intarray'])
            RT.extend([row['RT']]*len(row['intarray']))
            ms_lv.extend([row['MSlevel']] * len(row['intarray']))
            ms1_mz_min.extend([row['MS1_mz_min']] * len(row['intarray']))
            ms1_mz_max.extend([row['MS1_mz_max']] * len(row['intarray']))
        dico = {"RT": RT, "mz": mz, "inty": inty, "MSlevel": ms_lv, "MS1_mz_min": ms1_mz_min, "MS1_mz_max": ms1_mz_max}
        return pd.DataFrame(dico)

    else :
        return df

def generate_RT_int_imgs(exp,star_mz,stop_mz):
    exp.updateRanges()
    rt = []
    mz = []
    intensity = []
    for s in exp :
        if s.getMSLevel() == 1:
            rt.append(s.getRT())
            mz.append(s.get_peaks()[0])
            intensity.append(s.get_peaks()[1])
    mz_range = np.linspace(star_mz,stop_mz,1000)
    for i in range(len(mz_range)-1):
        print(mz_range[i],'/{}'.format(stop_mz))
        val = compute_chromatograms(rt, mz, intensity, mz_range[i] ,mz_range[i+1])
        fig, ax = plt.subplots()
        ax.plot(rt,val)
        ax.set_xlabel('Retention time')
        ax.set_ylabel('Intensity')
        ax.set_title('mz : {} to {}'.format(mz_range[i] ,mz_range[i+1]))
        plt.savefig('fig/rt_local/{}_to_{}.png'.format(mz_range[i] ,mz_range[i+1]))
        plt.clf()

def integrate_ms_ms(df, mz_bin, output='temp.png', display = False):
    # df_useful = df[(df['MS1 RT']>time_start) & (df['MS1 RT']<time_end) & (df['MSlevel']==2)].reset_index(inplace=True)
    min_mz, max_mz = min(df['mz']), max(df['mz'])
    mz_list = np.linspace(min_mz,max_mz,num = int((max_mz-min_mz)//mz_bin)+1)
    int_list = np.zeros(int((max_mz-min_mz)//mz_bin)+1)
    for index, row in df.iterrows():
        int_list[int((row['mz']-min_mz)//mz_bin)]+=row['inty']
    if display :
        plt.clf()
        fig, ax = plt.subplots()
        ax.plot(mz_list, int_list,linewidth=0.1)
        ax.set_xlim(200,1800)
        plt.savefig(output)
        plt.clf()
    return mz_list,int_list

def integrated_mz_int(mz_list,int_list,mz_bin, mz_ref):
    min_mz, max_mz = min(mz_ref), max(mz_ref)
    res = np.zeros(int((max_mz-min_mz)//mz_bin)+1)
    for i in range(len(mz_list)):
        res[int((mz_list[i] - min_mz) // mz_bin)] += int_list[i]
    return res

def intensity_coverage(int_expe,int_theo):
    coverage = np.sum(np.where(int_theo>0,int_expe,0))/np.sum(int_expe)
    return coverage

def window_and_filter(df, RT_min, RT_max, MS1_mz_min, MS2_mz_max, threshold):
    df_filtered = df[df['MSlevel'] == 2]
    df_filtered = df_filtered[MS1_mz_min < df['MS1_mz_max']]
    df_filtered = df_filtered[MS2_mz_max > df['MS1_mz_min']]
    df_filtered = df_filtered[RT_min < df['RT']]
    df_filtered = df_filtered[RT_max > df['RT']]
    df_filtered = df_filtered[df_filtered['inty']>threshold]
    return df_filtered

if __name__ == "__main__":
    e = oms.MSExperiment()
    oms.MzMLFile().load("data/echantillons données DIA/CITAMA-5-AER-d200.mzML", e)
    # generate_RT_int_imgs(e, 350, 1250)

    df = get_df(e, long=True)
    # df1 = df[df['MSlevel'] == 1]
    # df_slide = df1[411.5< df1['mz']]
    # df_slide = df_slide[418.5 > df_slide['mz']]
    #
    #
    # inty_sorted = [x for y, x in sorted(zip(df_slide['RT'], df_slide['inty']))]
    # mz_sorted = sorted(df_slide['RT'])
    # plt.clf()
    # fig, ax = plt.subplots()
    # ax.set_xlim(280,310)
    # ax.plot(mz_sorted,inty_sorted)
    #
    # plt.savefig('temp.png')
    #
#etude 1 : RT 403 env mz 586
#etude 2 : RT 417 env mz 620
    df_large_1 = df[df['MSlevel'] == 2]
    df_large_1 = df_large_1[583 < df_large_1['MS1_mz_max']]
    df_large_1 = df_large_1[590 > df_large_1['MS1_mz_min']]
    df_large_1 = df_large_1[400 < df_large_1['RT']]
    df_large_1 = df_large_1[407 > df_large_1['RT']]
    df_peak_1 = df[df['MSlevel'] == 2]
    df_peak_1 = df_peak_1[583 < df_peak_1['MS1_mz_max']]
    df_peak_1 = df_peak_1[590 > df_peak_1['MS1_mz_min']]
    df_peak_1 = df_peak_1[402.61 < df_peak_1['RT']]
    df_peak_1 = df_peak_1[404.70 > df_peak_1['RT']]

    peak_labels = ['y3','y4','y5','y6','y7','y8','y9','y10','y10 - H2O','b10']
    intensities_1 = [72.98152923583984,515.2999267578125,233.5383758544922,195.87428283691406,1547.2972412109375,565.280029296875,751.7384033203125,326.5546875,1582.326904296875,62.46744918823242,28.2091007232666,28.175247192382812,56.511573791503906,66.04779052734375,162.41555786132812,513.6303100585938,89.15145111083984,32.340614318847656]
    masses_1 = [260.19452231941415,317.21887057506785,416.28718638590306,529.3694932073394,658.4082871894378,757.4859952659489,886.516076743466,1015.5580941852136,1072.5841392915013,640.4013354636426,739.4767875172264,868.5072379164048,997.5687132366882,1054.5537160953004,286.1350031265497,415.1792848734148,514.2414463061315,912.4601973498769]

    mz1_large, inty1_large = integrate_ms_ms(df_large_1, 0.25)
    mz1, inty1 = integrate_ms_ms(df_peak_1, 0.25)


    intensities_1=np.array(intensities_1) / np.linalg.norm(intensities_1)
    inty1 = np.array(inty1) / np.linalg.norm(inty1)
    inty1_large = np.array(inty1_large) / np.linalg.norm(inty1_large)
    plt.clf()
    fig, ax = plt.subplots()
    ax.plot(mz1, inty1, linewidth=0.3)
    ax.vlines(masses_1, 0, -intensities_1, color="tab:red", linewidth=0.3)
    ax.set_xlim(200, 1200)
    plt.savefig('fig/local_inte_res/spec_peak_theo_1.png')

    plt.clf()
    fig, ax = plt.subplots()
    ax.plot(mz1_large, inty1_large, linewidth=0.3)
    ax.vlines(masses_1, 0, -intensities_1, color="tab:red", linewidth=0.3)
    ax.set_xlim(200, 1200)
    plt.savefig('fig/local_inte_res/spec_large_theo_1.png')
    plt.clf()

    int_theo_inte_peak_1 = integrated_mz_int(masses_1, intensities_1, 0.25, mz1)
    int_theo_inte_large_1 = integrated_mz_int(masses_1, intensities_1, 0.25, mz1_large)
    res_peak_1 = scipy.stats.pearsonr(int_theo_inte_peak_1, inty1)
    coverage_peak_1 = intensity_coverage(inty1,int_theo_inte_peak_1)
    res_large_1 = scipy.stats.pearsonr(int_theo_inte_large_1, inty1_large)
    coverage_large_1 = intensity_coverage(inty1_large, int_theo_inte_large_1)

    df_large_2 = df[df['MSlevel'] == 2]
    df_large_2 = df_large_2[619 < df_large_2['MS1_mz_max']]
    df_large_2 = df_large_2[623 > df_large_2['MS1_mz_min']]
    df_large_2 = df_large_2[414.8 < df_large_2['RT']]
    df_large_2 = df_large_2[420.3 > df_large_2['RT']]

    df_peak_2 = df[df['MSlevel'] == 2]
    df_peak_2 = df_peak_2[619 < df_peak_2['MS1_mz_max']]
    df_peak_2 = df_peak_2[623 > df_peak_2['MS1_mz_min']]
    df_peak_2 = df_peak_2[417.2 < df_peak_2['RT']]
    df_peak_2 = df_peak_2[419.3 > df_peak_2['RT']]
    mz2, inty2 = integrate_ms_ms(df_peak_2, 0.25)
    mz2_large, inty2_large = integrate_ms_ms(df_large_2, 0.25)

    intensities_2 = [28.185697555541992,190.35972595214844,369.7372741699219,218.33245849609375,450.749755859375,289.6231994628906,282.96661376953125,151.11683654785156,56.205596923828125,151.97030639648438,145.062255859375,43.23415756225586]
    masses_2 = [246.1807143935358,560.2988894900997,673.3880444971912,730.3965872674734,801.4428960350862,872.4753078336571,943.5086099811606,1056.596262153857,1038.5789221617815,298.2142932725484,369.2477520599847,440.2814643814369]


    intensities_2=np.array(intensities_2) / np.linalg.norm(intensities_2)
    inty2 = np.array(inty2) / np.linalg.norm(inty2)
    inty2_large = np.array(inty2_large) / np.linalg.norm(inty2_large)

    plt.clf()
    fig, ax = plt.subplots()
    ax.plot(mz2, inty2, linewidth=0.3)
    ax.vlines(masses_2, 0, -intensities_2, color="tab:red", linewidth=0.3)
    ax.set_xlim(200, 1200)
    plt.savefig('fig/local_inte_res/spec_peak_theo_2.png')
    plt.clf()

    plt.clf()
    fig, ax = plt.subplots()
    ax.plot(mz2_large, inty2_large, linewidth=0.3)
    ax.vlines(masses_2, 0, -intensities_2, color="tab:red", linewidth=0.3)
    ax.set_xlim(200, 1200)
    plt.savefig('fig/local_inte_res/spec_large_theo_2.png')
    plt.clf()

    int_theo_inte_peak_2 = integrated_mz_int(masses_2, intensities_2, 0.25, mz2)
    int_theo_inte_large_2 = integrated_mz_int(masses_2, intensities_2, 0.25, mz2_large)
    res_peak_2 = scipy.stats.pearsonr(int_theo_inte_peak_2, inty2)
    coverage_peak_2 = intensity_coverage(inty2,int_theo_inte_peak_2)
    res_large_2 = scipy.stats.pearsonr(int_theo_inte_large_2, inty2_large)
    coverage_large_2 = intensity_coverage(inty2_large, int_theo_inte_large_2)


