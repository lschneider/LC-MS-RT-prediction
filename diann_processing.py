import pandas as pd

spec_lib = pd.read_parquet('database/CIT_BASE_UP000584719_546.parquet')
lib_rt = spec_lib[['Stripped.Sequence','RT']]
df = lib_rt.groupby(['Stripped.Sequence'])['RT'].mean().to_frame().reset_index()
df.rename(columns = {'Stripped.Sequence':'Sequence', 'RT':'Retention time'}, inplace = True)
df.to_csv('database/CIT_BASE_UP000584719_546.csv')

