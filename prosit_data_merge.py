import pandas as pd
import numpy as np
from common_dataset import Common_Dataset
from dataloader import load_intensity_df_from_files


import numpy as np
import torch
from torch.utils.data import Dataset, DataLoader
import pandas as pd

ALPHABET_UNMOD = {
    "_": 0,
    "A": 1,
    "C": 2,
    "D": 3,
    "E": 4,
    "F": 5,
    "G": 6,
    "H": 7,
    "I": 8,
    "K": 9,
    "L": 10,
    "M": 11,
    "N": 12,
    "P": 13,
    "Q": 14,
    "R": 15,
    "S": 16,
    "T": 17,
    "V": 18,
    "W": 19,
    "Y": 20,
    "CaC": 21,
    "OxM": 22
}


def padding(dataframe, columns, length):
    def pad(x):
        return x + (length - len(x) + 2 * x.count('-')) * '_'

    for i in range(len(dataframe)):
        if len(dataframe[columns][i]) > length + 2 * dataframe[columns][i].count('-'):
            dataframe.drop(i)

    dataframe[columns] = dataframe[columns].map(pad)

def zero_to_minus(arr):
    arr[arr <= 0.00001] = -1.
    return arr

def alphabetical_to_numerical(seq):
    num = []
    dec = 0
    for i in range(len(seq) - 2 * seq.count('-')):
        if seq[i+dec] != '-':
            num.append(ALPHABET_UNMOD[seq[i+dec]])
        else:
            if seq[i + dec + 1:i + dec + 4] == 'CaC':
                num.append(21)
            elif seq[i + dec + 1:i + dec + 4] == 'OxM':
                num.append(22)
            else:
                raise 'Modification not supported'
            dec += 4
    return np.array(num)

if __name__ == '__main__' :

    # sources = ('data/intensity/sequence_train.npy',
    #              'data/intensity/intensity_train.npy',
    #              'data/intensity/precursor_charge_train.npy',
    #              'data/intensity/precursor_charge_train.npy')
    #
    #
    # data_rt = pd.read_csv('database/data_unique_ptms.csv')
    # data_rt['Sequence']=data_rt['mod_sequence']
    #
    # padding(data_rt, 'Sequence', 30)
    # data_rt['Sequence'] = data_rt['Sequence'].map(alphabetical_to_numerical)
    #
    # data_rt =data_rt.drop(columns='mod_sequence')
    #
    # data_int = load_intensity_df_from_files(sources[0], sources[1], sources[2], sources[3])
    #
    # seq_rt = data_rt.Sequence
    # seq_int = data_int.seq
    # seq_rt = seq_rt.tolist()
    # seq_int = seq_int.tolist()
    # seq_rt = [tuple(l) for l in seq_rt]
    # seq_int = [tuple(l) for l in seq_int]
    #
    # ind_dict_rt = dict((k, i) for i, k in enumerate(seq_rt))
    # inter = set(ind_dict_rt).intersection(seq_int)
    # ind_dict_rt = [ind_dict_rt[x] for x in inter]
    #
    #
    # data_int.irt = np.zeros(data_int.energy.shape)
    #
    # i=0
    # for ind in ind_dict_rt :
    #     print(i,'/',len(ind_dict_rt))
    #     i+=1
    #     ind_int = [k for k, x in enumerate(seq_int) if x == seq_rt[ind]]
    #     data_int.irt[ind_int] = data_rt.irt[ind]
    #
    # np.save('data/intensity/irt_train.npy',data_int.irt)


    # sources = ('data/intensity/sequence_holdout.npy',
    #              'data/intensity/intensity_holdout.npy',
    #              'data/intensity/precursor_charge_holdout.npy',
    #              'data/intensity/precursor_charge_holdout.npy')
    #
    #
    # data_rt = pd.read_csv('database/data_unique_ptms.csv')
    # data_rt['Sequence']=data_rt['mod_sequence']
    #
    # padding(data_rt, 'Sequence', 30)
    # data_rt['Sequence'] = data_rt['Sequence'].map(alphabetical_to_numerical)
    #
    # data_rt =data_rt.drop(columns='mod_sequence')
    #
    # data_int = load_intensity_df_from_files(sources[0], sources[1], sources[2], sources[3])
    #
    # seq_rt = data_rt.Sequence
    # seq_int = data_int.seq
    # seq_rt = seq_rt.tolist()
    # seq_int = seq_int.tolist()
    # seq_rt = [tuple(l) for l in seq_rt]
    # seq_int = [tuple(l) for l in seq_int]
    #
    # ind_dict_rt = dict((k, i) for i, k in enumerate(seq_rt))
    # inter = set(ind_dict_rt).intersection(seq_int)
    # ind_dict_rt = [ind_dict_rt[x] for x in inter]
    #
    #
    # data_int.irt = np.zeros(data_int.energy.shape)
    #
    # i=0
    # for ind in ind_dict_rt :
    #     print(i,'/',len(ind_dict_rt))
    #     i+=1
    #     ind_int = [k for k, x in enumerate(seq_int) if x == seq_rt[ind]]
    #     data_int.irt[ind_int] = data_rt.irt[ind]
    #
    # np.save('data/intensity/irt_holdout.npy',data_int.irt)

    # df = pd.read_pickle('database/data_prosit_merged_holdout.pkl')
    # print(len(df))
    # possible_charges = [1,2,3,4]
    # df = df[df['Charge'].isin(possible_charges)]
    # print(len(df))
    # df.to_pickle('database/data_prosit_merged_holdout.pkl')
    # df = pd.read_pickle('database/data_prosit_merged_train.pkl')
    # print(len(df))
    # df=df.loc[df['Retention time']!=0]
    # df = df[df['Charge'].isin(possible_charges)]
    # print(len(df))
    # df.to_pickle('database/data_prosit_merged_train.pkl')
    pass

