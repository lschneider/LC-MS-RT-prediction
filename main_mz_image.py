import numpy as np
import pyopenms as oms
from mzml_exploration import build_image_generic
import os
import glob


DIR_NAME = 'data/mzml/*.mzml'
BIN_SIZE = 2
NUM_RT = 300

if '__name__' == '__main__':

    l = glob.glob(DIR_NAME, root_dir=None, dir_fd=None, recursive=False, include_hidden=False)
    for f_name in l :
        e = oms.MSExperiment()
        oms.MzMLFile().load(f_name, e)
        im = build_image_generic(e, BIN_SIZE, NUM_RT)
        im2 = np.maximum(0, np.log(im + 1))
        np.save(os.path.splitext('f_name')[0] + '.npy',im2)
