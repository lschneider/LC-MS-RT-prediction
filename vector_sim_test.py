import numpy as np
import matplotlib.pyplot as plt

def cos_sim(a,b):
    return (a.dot(b))/(np.linalg.norm(a)*np.linalg.norm(b))

l10=[]
l100=[]
for _ in range(1000):
    vec10 = np.random.random(10)
    vec10b = vec10 + np.random.random(10)

    l10.append(cos_sim(vec10,vec10b))

    vec100 = np.random.random(100)
    vec100b = vec100 + np.random.random(100)

    l100.append(cos_sim(vec100,vec100b))


fig, ax = plt.subplots()

ax.plot(l10,c='b',ls='',marker='.')
ax.plot(l100,c='y',ls='',marker='.')
plt.savefig('temp.png')
print(np.mean(l10))
print(np.mean(l100))