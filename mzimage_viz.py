import nibabel as nib
import numpy as np


from nilearn import plotting

data = np.load('data/mz_image/Staph140.npy')

new_image = nib.Nifti1Image(data, affine=np.eye(4))



plotting.plot_stat_map(
    new_image,
    bg_img=None,
    display_mode="x",
    cut_coords=1,
    threshold=4,
    title="Test",
)