import pandas as pd
from data_exploration import dist_long, feq_aa


# Enterobac = pd.read_excel('database/Enterobac.xlsx')
# Enterococcus = pd.read_excel('database/Enterococcus.xlsx')
# Pyo = pd.read_excel('database/Pyo.xlsx')
# Staph = pd.read_excel('database/Staph.xlsx')
#
# Enterobac['id']='Enterobac'
# Enterococcus['id']='Enterococcus'
# Pyo['id']='Pyo'
# Staph['id']='Staph'
#
# res = pd.concat([Enterobac, Enterococcus, Pyo, Staph], ignore_index=True)
#
# id = pd.read_excel('database/All_Peptides.xlsx')
#
# res.to_csv('database/peptides_res.csv')
# id.to_csv('database/peptides_id.csv')

res = pd.read_csv('database/peptides_res.csv')
id = pd.read_csv('database/peptides_id.csv')

dist_res = dist_long(res['Peptides'], plot=False, save=True, f_name='fig/res_dist_long.png')
dist_id = dist_long(id['Peptides'], plot=False, save=True, f_name='fig/id_dist_long.png')

freq_res = feq_aa(res['Peptides'], plot=False, save=True, f_name='fig/res_feq_aa.png')
freq_id = feq_aa(id['Peptides'], plot=False, save=True, f_name='fig/id_feq_aa.png')