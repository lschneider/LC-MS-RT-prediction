import random
import matplotlib
import numpy as np
import matplotlib.pyplot as plt
from dlomix.data import RetentionTimeDataset
import pandas as pd
import Levenshtein as lv
epoch = 100
number = 8
BATCH_SIZE=64

test_rtdata = RetentionTimeDataset(data_source='database/data_holdout.csv',
                                       seq_length=30, batch_size=BATCH_SIZE, test=True)

rtdata = RetentionTimeDataset(data_source='database/data_train.csv',
                                  seq_length=30, batch_size=BATCH_SIZE, val_ratio=0.2, test=False)

data_train = pd.read_csv('database/data_train.csv')
data_test = pd.read_csv('database/data_holdout.csv')
train_seq = data_train['sequence']
test_seq = data_test['sequence']

target = test_rtdata.get_split_targets(split="test")
index = random.sample(range(target.size), number)
target_plot = target[index]
pred_1=[]
pred_2=[]
error_1=[]
error_2=[]
order_1=[]
order_2=[]
pred_plot_1=[]
pred_plot_2=[]
error_plot_1=[]
error_plot_2=[]
order_plot_1=[]
order_plot_2=[]


for i in range(epoch):
    data_1 = np.load('results/pred_prosit_ori/mem_pred_'+str(i)+'.npy')
    data_1_plot =data_1[index]
    data_2 = np.load('results/pred_prosit_ori_2/mem_pred_' + str(i) + '.npy')
    data_2_plot = data_2[index]
    pred_1.append(data_1)
    pred_2.append(data_2)
    error_1.append(data_1-target)
    error_2.append(data_2-target)
    order_1.append(np.argsort(data_1-target))
    order_2.append(np.argsort(data_2-target))
    pred_plot_1.append(data_1_plot)
    pred_plot_2.append(data_2_plot)
    error_plot_1.append(data_1_plot-target_plot)
    error_plot_2.append(data_2_plot-target_plot)
    order_plot_1.append(np.argsort(data_1_plot-target_plot))
    order_plot_2.append(np.argsort(data_2_plot-target_plot))

pred_1=np.array(pred_1)
pred_2=np.array(pred_2)
error_1=np.array(error_1)
error_2=np.array(error_2)
order_1=np.array(order_1)
order_2=np.array(order_2)
pred_plot_1=np.array(pred_plot_1)
pred_plot_2=np.array(pred_plot_2)
error_plot_1=np.array(error_plot_1)
error_plot_2=np.array(error_plot_2)
order_plot_1=np.array(order_plot_1)
order_plot_2=np.array(order_plot_2)

def viz_error_1():
    for i in range(number):
        plt.plot(error_plot_1[:,i])
    plt.show()

def viz_error_2():
    for i in range(number):
        plt.plot(error_plot_2[:,i])
    plt.show()

def compare_error():
    colors = list(matplotlib.colors.TABLEAU_COLORS)
    for i in range(number):
        plt.plot(error_plot_1[:,i], c = colors[i], ls =  'dashed')
        plt.plot(error_plot_2[:, i], c = colors[i])
    plt.show()

def get_worse_seq(n,epoch):
    error = error_1[epoch,:]
    ind = np.argpartition(error, n)[:n]
    return test_seq[ind].to_list(),ind

def get_nb_iteration(seq_list):
    nb_list=[]
    counts = train_seq.value_counts()
    for seq in seq_list:
        nb_list.append(counts[seq])


def compute_min_distance(seq_list):
    res=[]
    for ref in seq_list :
        min_dist = 10000
        for seq in train_seq:
            d = 1 - lv.ratio(seq,ref)
            if d < min_dist:
                min_dist = d
        res.append(min_dist)
    return res

radom_1000 = index = random.sample(range(test_seq.size), 1000)

